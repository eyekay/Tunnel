'''
Tunnel Browser

Browser Window - User Interface
'''

# SPDX-License-Identifier: GPL-3.0-or-later

import os

import gi
gi.require_version('Adw', '1')
from gi.repository import Gtk, GLib, WebKit, Gdk, GdkPixbuf, Gio, Pango, Adw

icons_path = __file__.rpartition(os.path.sep)[0] + '/data/'

def init_browser_window(self, app, url, incognito):
    self.progress = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
    self.progressbar = Gtk.ProgressBar()
    self.progressbar.add_css_class("osd")
    self.progress.append(self.progressbar)

    toolbarview = Adw.ToolbarView()
    headerbar = Adw.HeaderBar()
    toolbarview.add_top_bar(headerbar)
    
    headerbox = Gtk.Box()
    headerbox.add_css_class("toolbar")

    toolbarview.add_top_bar(headerbox)
    toolbarview.add_top_bar(self.progress)

    self.bookmarks_bar = Gtk.Box()
    self.bookmarks_bar.add_css_class("toolbar")
    toolbarview.add_top_bar(self.bookmarks_bar)

    webbox = Gtk.Paned()  
    webbox.set_shrink_start_child(False)
    webbox.set_position(1)
    webbox.set_wide_handle(True)

    self.backbutton = Gtk.Button()
    self.backbutton.set_icon_name("left-large-symbolic")
    self.backbutton.set_tooltip_text('Back (Alt+Left)')
    self.backbutton.set_sensitive(False)
    headerbox.append(self.backbutton)
    self.forwardbutton = Gtk.Button()
    self.forwardbutton.set_icon_name("right-large-symbolic")
    self.forwardbutton.set_tooltip_text('Forward (Alt+Right)')
    self.forwardbutton.set_sensitive(False)
    headerbox.append(self.forwardbutton)
    self.reloadbutton = Gtk.Button()
    self.reloadbutton.set_icon_name("arrow-circular-top-right-symbolic")
    self.reloadbutton.set_tooltip_text('Reload (Ctrl+R)')
    headerbox.append(self.reloadbutton)
    self.home_button = Gtk.Button()
    self.home_button.set_icon_name("go-home-symbolic")
    self.home_button.set_tooltip_text('Home')
    headerbox.append(self.home_button)
    self.favicon_image = Gtk.Image()
    headerbox.append(self.favicon_image)
    self.entry = Gtk.Entry()
    self.entry.set_can_focus(True)
    self.entry.set_tooltip_text('Enter URL or Search')
    self.entry.set_hexpand(True)
    headerbox.append(self.entry)

    self.menu = Gio.Menu()

    app.create_action('bookmark', lambda x,y: self.bookmark, ['<primary>b'])
    self.menu.append("Bookmark", "app.bookmark")
    app.create_action('new_window', lambda x, y: self.create_new_window(app = app, incognito = False), ['<primary>n'])
    self.menu.append("New Window", "app.new_window")
    app.create_action('new_incognito_window', lambda x, y: self.create_new_window(app = app, incognito = True), ['<primary><Shift>n'])
    self.menu.append("New Incognito Window", "app.new_incognito_window")
    app.create_action('close_window', lambda x, y: app.get_active_window().close(), ['<primary>w'])
    self.menu.append("Close Window", "app.close_window")
    app.create_action('save', self.on_save_button_clicked, ['<primary>s'])
    self.menu.append("Save", "app.save")
    app.create_action('print', self.on_print_button_clicked, ['<primary>p'])
    self.menu.append("Print", "app.print")
    app.create_action('hist', self.on_hist_button_clicked, ['<primary>h'])
    self.menu.append("History", "app.hist")
    app.create_action('clear_hist', self.on_clear_hist_button_clicked)
    self.menu.append("Clear History", "app.clear_hist")
    app.create_action('zoom_in', lambda x, y: self.webview.set_zoom_level(self.webview.get_zoom_level() + 0.1), ['<primary>plus'])
    self.menu.append("Zoom In", "app.zoom_in")
    app.create_action('zoom_out', lambda x, y: self.webview.set_zoom_level(self.webview.get_zoom_level() - 0.1), ['<primary>minus'])
    self.menu.append("Zoom Out", "app.zoom_out")
    app.create_action('zoom_normal', lambda x, y: self.webview.set_zoom_level(1.0), ['<primary>0'])
    self.menu.append("Zoom Normal", "app.zoom_normal")
    app.create_action('find', self.show_find_bar, ['<primary>f'])
    self.menu.append("Find", "app.find")
    app.create_action('settings', lambda x, y: self.show_settings_dialog(None, app))
    self.menu.append("Settings", "app.settings")
    app.create_action('about', lambda x, y: self.show_about_dialog(app))
    self.menu.append("About", "app.about")
    app.create_action('quit', lambda x, y: app.quit(), ['<primary>q'])
    self.menu.append("Quit", "app.quit")

    self.menu_button = Gtk.MenuButton()
    self.menu_button.set_menu_model(self.menu)
    self.menu_button.set_icon_name("open-menu-symbolic")
    self.menu_button.set_tooltip_text('Menu')
    
    headerbox.append(self.menu_button)
    scrolled_window = Gtk.ScrolledWindow()
    scrolled_window.set_child(self.webview)
    self.webview.set_vexpand(True)

    self.find_bar = Gtk.Box()
    self.find_bar_close_button = Gtk.Button()
    self.find_bar_close_button.set_icon_name("window-close-symbolic")
    
    self.find_bar.append(self.find_bar_close_button)
    self.find_entry = Gtk.Entry()
    self.find_entry.set_tooltip_text('Enter text to find')
    self.find_bar.append(self.find_entry)
    self.case_sensitive_button = Gtk.CheckButton()
    self.case_sensitive_button.set_label('Case Sensitive')
    self.find_bar.append(self.case_sensitive_button)
    self.wrap_button = Gtk.CheckButton()
    self.wrap_button.set_label('Wrap Around')
    self.find_bar.append(self.wrap_button)
    self.word_button = Gtk.CheckButton()
    self.word_button.set_label('At Word Start')
    self.find_bar.append(self.word_button)
    self.backwards_button = Gtk.CheckButton()
    self.backwards_button.set_label('Backwards')
    self.find_bar.append(self.backwards_button)
    self.search_next_button = Gtk.Button()
    self.search_next_button.set_label('Next')
    self.find_bar.append(self.search_next_button)

    vbox = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
    webbox.set_end_child(scrolled_window)
    vbox.append(webbox)
    vbox.append(self.find_bar)
    toolbarview.set_content(vbox)
    self.set_content(toolbarview)