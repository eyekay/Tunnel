'''
Tunnel Browser

Browser Window - Non-UI Functions

None of these functions need self.
'''

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import time
import sqlite3
import mmap
import urllib
import urllib.parse
import requests

from . import easylist_conv

def remove_if_starts_with(text, prefix) -> str:
    if text.startswith(prefix):
        return text[len(prefix):]
    else:
        return text

def update_history_bar(store) -> None:
    history = set()

    con = sqlite3.connect(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/history.db'))
    cur = con.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS history (url varchar(1024), title varchar(255), datetime varchar(50));')
    cur.execute('SELECT url FROM history')
    for i in cur:
        history.add(i[0])
        history.add(remove_if_starts_with(remove_if_starts_with(remove_if_starts_with(i[0], 'https://'), 'http://'), 'www.'))
        history.add(remove_if_starts_with(remove_if_starts_with(i[0], 'https://'), 'http://'))
        bare_domain = remove_if_starts_with(remove_if_starts_with(i[0], 'https://'), 'http://').split('.')
        if len(bare_domain) > 2:
            history.add(bare_domain[-2] + '.' + bare_domain[-1])
        history.add(get_domain_name(i[0]))

    for i in list(history)[::-1]:
        store.append([i])

def get_navigable_url(url, SEARCH_URL) -> str:
    if os.path.exists(url):
        url = 'file://' + url
    elif is_valid_url(url):
        url = url
    elif is_valid_url('https://' + url):
        url = 'https://' + url
    elif url == 'about:blank':
        pass
    else:
        url = search(url, SEARCH_URL)
    return url

def update_filters(adblocking, FILTER_PATH, FILTER_CSS_PATH): # This one runs in a thread
    if adblocking:
        if os.path.exists(FILTER_PATH) and os.path.getsize(FILTER_PATH) > 0:
            with open(FILTER_PATH, 'r') as f:
                wtime = f.readline()[2:]
            if float(time.time()) - float(wtime) > 259200: # 3 days
                hosts = requests.get('https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts').text
                with open(FILTER_PATH, 'w') as f:
                    f.write('# ' + str(time.time()) + '\n')
                    f.write(hosts)
        else:
            hosts = requests.get('https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts').text
            with open(FILTER_PATH, 'w') as f:
                f.write('# ' + str(time.time()) + '\n')
                f.write(hosts)
        
        if os.path.exists(FILTER_CSS_PATH) and os.path.getsize(FILTER_PATH) > 0:
            with open(FILTER_CSS_PATH, 'r') as f:
                wtime = f.readline()[3:-3]
            if float(time.time()) - float(wtime) > 259200: # 3 days
                hosts = requests.get('https://easylist.to/easylist/easylist.txt').text
                easylist_conv.convert(hosts, FILTER_CSS_PATH)
        else:
            hosts = requests.get('https://easylist.to/easylist/easylist.txt').text
            easylist_conv.convert(hosts, FILTER_CSS_PATH)

def is_to_be_filtered(domain, FILTER_PATH) -> bool:
    with open(FILTER_PATH, 'r') as f:
        with mmap.mmap(f.fileno(), 0, access=mmap.ACCESS_READ) as file_mmap:
            if file_mmap.find(domain.encode()) != -1:
                return True
    return False

def is_valid_url(url) -> bool:
    parsed_url = urllib.parse.urlparse(url)
    if parsed_url.scheme != '' and parsed_url.netloc != '' and url.find('.') != -1 and url.find(' ') == -1:
        return True
    else:
        return False

def search(text, SEARCH_URL) -> str:
    converted_text = urllib.parse.quote(text)
    converted_text = SEARCH_URL + converted_text
    return converted_text

def get_domain_name(url) -> str:
    parsed_url = urllib.parse.urlparse(url)
    domain_name = parsed_url.netloc
    return domain_name

