'''
Tunnel Browser

Download Window
'''

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import requests
import threading

import gi

from gi.repository import Gtk, GLib

class DownloadWindow(Gtk.Window):
    def __init__(self, app, url, path):
        show_url = url[:255]
        if len(url) > 255:
            show_url += '...'
        Gtk.Window.__init__(self, title="Downloading - " + show_url, application = app)

        #self.set_icon_from_file(icons_path + 'tunnel_icon.png')

        self.set_default_size(300, 50)
        self.set_resizable(False)
        self.set_position(Gtk.WindowPosition.CENTER)

        box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)

        label = Gtk.Label(label = 'Downloading ' + show_url + ' ...')
        box.pack_start(label, False, False, 0)

        self.content_length_label = Gtk.Label(label = '')
        box.pack_start(self.content_length_label, False, False, 0)
        self.progressbar = Gtk.ProgressBar()
        self.progressbar.set_show_text(True)
        self.progressbar.set_text('0%')
        box.pack_start(self.progressbar, False, False, 0)

        cancel_box = Gtk.Box()
        space_filler = Gtk.Label(label = '')
        cancel_box.pack_start(space_filler, True, True, 0)
        self.cancel_button = Gtk.Button()
        self.cancel_button.set_label("Cancel")
        self.cancel_button.connect('clicked', self.cancel)
        cancel_box.pack_start(self.cancel_button, False, False, 0)
        box.pack_start(cancel_box, False, False, 0)

        self.add(box)

        self.connect('destroy', self.cancel)

        self.cancelled = False

        self.start_download(url, path)
        
    def start_download(self, url, path):
        thread = threading.Thread(target=self._download_thread, args=(url, path))
        thread.start()

    def _download_thread(self, url, path):
        response = requests.get(url, stream=True)

        file_size = int(response.headers.get("content-length", 0))
        self.content_length_label.set_text('Total size: ' + str(round(file_size / 1024 / 1024, 2)) + ' MB')
        block_size = 1024
        downloaded_size = 0

        with open(path, "wb") as file:
            for data in response.iter_content(block_size):
                if self.cancelled:
                    self.destroy()
                    return

                downloaded_size += len(data)
                file.write(data)
                GLib.idle_add(self.update_progress, downloaded_size, file_size)

        if not self.cancelled:
            GLib.idle_add(self.complete_download)

    def cancel(self, button):
        self.cancelled = True

    def update_progress(self, downloaded, total):
        progress = float(downloaded) / float(total) if float(total) > 0 else 0
        # Sometimes for some reason the website doesn't think you are privileged enough to know the total size
        self.progressbar.set_fraction(progress)
        self.progressbar.set_text("{:.0%}".format(progress))

    def complete_download(self):
        self.destroy()