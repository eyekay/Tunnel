'''
Tunnel Browser

Browser Window - Connections
'''

# SPDX-License-Identifier: GPL-3.0-or-later

from .browser_window_signals import *

def set_connections(self, app, HISTORY_FILE, START_PAGE):
    self.backbutton.connect("clicked", self.on_back_clicked)
    app.create_action('back', self.on_back_clicked, shortcuts=['<alt>Left'])
    self.forwardbutton.connect("clicked", self.on_forward_clicked)
    app.create_action('forward', self.on_forward_clicked, shortcuts=['<alt>Right'])
    self.reloadbutton.connect("clicked", self.on_reload_clicked)
    app.create_action('reload', self.on_reload_clicked, shortcuts=['<Control>r'])
    self.home_button.connect("clicked", lambda x: self.navigate(START_PAGE))
    self.entry.connect("activate", self.on_entry_activated)

    self.menu_button.connect("activate", self.on_menu_button_clicked)

    self.find_bar_close_button.connect('clicked', self.close_find_bar)
    self.find_entry.connect('activate', self.find_text)
    self.search_next_button.connect('clicked', self.find_text)

    self.webview.connect("load-changed", lambda webview, event: on_load_changed(self, webview, event, self.HISTORY_FILE, self.FILTER_CSS_PATH))
    self.webview.connect('notify::estimated-load-progress', self.on_load_progress)
    self.webview.connect("create", on_create, self, app)
    self.webview.connect("decide-policy", lambda web_view, decision, decision_type: on_decide_policy(self, app, web_view, decision, decision_type, self.FILTER_PATH))
    self.webview.connect("context-menu", self.on_context_menu)
    self.webview.connect("enter-fullscreen", lambda x: headerbox.set_visible(False))
    self.webview.connect("leave-fullscreen", lambda x: headerbox.set_visible(True))
    self.webview.connect("permission-request", lambda webview, request: on_permission_request(self, webview, request, self.SETTINGS_PATH))
    self.webview.connect("notify::favicon", self.set_new_favicon)
