'''
Tunnel Browser

Settings Window
'''

# SPDX-License-Identifier: GPL-3.0-or-later

import os

import gi

from gi.repository import Gtk

class SettingsWindow(Gtk.Window):
    def __init__(self, app):
        Gtk.Window.__init__(self, title="Settings - Tunnel", application = app)
        #self.set_icon_from_name('tunnel_icon')

        self.set_default_size(300, 200)
        self.set_resizable(False)

        box = Gtk.Box(orientation = Gtk.Orientation.VERTICAL)
        hbox = Gtk.Box()
        image = Gtk.Image()
        image.set_from_icon_name('net.codelogistics.tunnel')
        image.set_pixel_size(72)
        hbox.append(image)
        heading = Gtk.Label(label = '\tSettings')
        hbox.append(heading)
        box.append(hbox)
        separator = Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL)
        box.append(separator)

        if os.path.exists(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/prefs.conf')):
            prefs = open(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/prefs.conf'), 'r').read().split('\n')
            for i in prefs:
                if i.startswith('start_page'):
                    START_PAGE = i.partition('=')[2].strip()
                if i.startswith('cookies_path'):
                    COOKIES_PATH = i.partition('=')[2].strip()
                if i.startswith('search_url'):
                    SEARCH_URL = i.partition('=')[2].strip()
                if i.startswith('history_page'):
                    HISTORY_FILE = i.partition('=')[2].strip()
                if i.startswith('incognito'):
                    incognito = i.split('=')[1].strip()
                    if incognito == "True":
                        incognito = True
                    else:
                        incognito = False
                if i.startswith('adblocking'):
                    adblocking = i.split('=')[1].strip()
                    if adblocking == "True":
                        adblocking = True
                    else:
                        adblocking = False
        else:
            incognito = False
            adblocking = True
            START_PAGE = 'https://start.duckduckgo.com/'
            COOKIES_PATH = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/cookies.txt')
            SEARCH_URL = 'https://duckduckgo.com/?q='
            HISTORY_FILE = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/history.db')

        label = Gtk.Label()
        label.set_markup('<i>Please note that the changes you make here will be applied after you restart Tunnel.</i>')
        box.append(label)

        gridbox = Gtk.Box()
        packing = Gtk.Label(label='\t')
        gridbox.append(packing)
        grid = Gtk.Grid()
        grid.set_row_spacing(10)
        grid.set_column_spacing(10)

        hlabel = Gtk.Label(label = 'Home page')
        self.start_page_entry = Gtk.Entry()
        self.start_page_entry.set_text(START_PAGE)
        self.start_page_entry.set_hexpand(True)
        slabel = Gtk.Label(label = 'Search engine URL')
        self.cookies_path_entry = Gtk.Entry()
        self.cookies_path_entry.set_text(COOKIES_PATH)
        clabel = Gtk.Label(label = 'Cookie jar path')
        self.search_url_entry = Gtk.Entry()
        self.search_url_entry.set_text(SEARCH_URL)
        hflabel = Gtk.Label(label = 'History file path')
        self.history_page_entry = Gtk.Entry()
        self.history_page_entry.set_text(HISTORY_FILE)
        ilabel = Gtk.Label(label = 'Incognito at Startup')
        self.incognito_switch = Gtk.Switch()
        self.incognito_switch.set_halign(Gtk.Align.START)
        self.incognito_switch.set_active(incognito)
        alabel = Gtk.Label(label = 'Ad blocking at Startup')
        self.adblocking_switch = Gtk.Switch()
        self.adblocking_switch.set_halign(Gtk.Align.START)
        self.adblocking_switch.set_active(adblocking)

        grid.attach(hlabel, 0, 0, 1, 1)
        grid.attach(self.start_page_entry, 1, 0, 1, 1)
        grid.attach(slabel, 0, 1, 1, 1)
        grid.attach(self.search_url_entry, 1, 1, 1, 1)
        grid.attach(clabel, 0, 2, 1, 1)
        grid.attach(self.cookies_path_entry, 1, 2, 1, 1)
        grid.attach(hflabel, 0, 3, 1, 1)
        grid.attach(self.history_page_entry, 1, 3, 1, 1)
        grid.attach(ilabel, 0, 4, 1, 1)
        grid.attach(self.incognito_switch, 1, 4, 1, 1)
        grid.attach(alabel, 0, 5, 1, 1)
        grid.attach(self.adblocking_switch, 1, 5, 1, 1)

        gridbox.append(grid)
        packing = Gtk.Label(label='\t')
        gridbox.append(packing)

        box.append(gridbox)
        apply_button = Gtk.Button(label='Apply')
        apply_button.connect('clicked', self.apply)
        box.append(apply_button)

        start_page_completion = Gtk.EntryCompletion()

        start_page_completion_store = Gtk.ListStore(str)
        for i in ['https://start.duckduckgo.com/',
        'https://duckduckgo.com/',
        'https://google.com/',
        'https://bing.com/',
        'https://search.brave.com/',
        'https://www.ecosia.org/',
        'https://startpage.com/',
        'about:blank/']:
            start_page_completion_store.append([i])

        start_page_completion.set_model(start_page_completion_store)
        start_page_completion.set_text_column(0)
        start_page_completion.set_inline_completion(True)
        start_page_completion.set_minimum_key_length(0)

        self.start_page_entry.set_completion(start_page_completion)

        search_engine_completion = Gtk.EntryCompletion()

        search_engine_completion_store = Gtk.ListStore(str)
        for i in ['https://duckduckgo.com/?q=',
        'https://google.com/search?q=',
        'https://bing.com/search?q=',
        'https://search.brave.com/search?q=',
        'https://www.ecosia.org/search?q=',
        'https://www.startpage.com/sp/search?query=']:
            search_engine_completion_store.append([i])

        search_engine_completion.set_model(search_engine_completion_store)
        search_engine_completion.set_text_column(0)
        search_engine_completion.set_inline_completion(True)
        search_engine_completion.set_minimum_key_length(0)

        self.search_url_entry.set_completion(search_engine_completion)        

        self.set_child(box)
    
    def apply(self, widget):
        with open(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/prefs.conf'), 'w') as f:
            f.write('start_page=' + self.start_page_entry.get_text() + '\n')
            f.write('cookies_path=' + self.cookies_path_entry.get_text() + '\n')
            f.write('search_url=' + self.search_url_entry.get_text() + '\n')
            f.write('history_page=' + self.history_page_entry.get_text() + '\n')
            f.write('incognito=' + str(self.incognito_switch.get_active()) + '\n')
            f.write('adblocking=' + str(self.adblocking_switch.get_active()) + '\n')
