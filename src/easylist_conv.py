'''
Tunnel Browser

Substandard Adblockplus/ Easylist to CSS Converter

Does not handle domains
'''

# SPDX-License-Identifier: GPL-3.0-or-later

import re
import time

def convert_line(filter_rule):
    filter_rule = filter_rule.strip()

    if '##' in filter_rule:
        filter_rule = filter_rule.split('##', 1)[-1]

    if '###' in filter_rule:
        filter_rule = filter_rule.split('###', 1)[-1]
    filter_rule = re.sub(r'(?<!\\)-', r'\-', filter_rule)

    return filter_rule

def convert(filter_list, output_css):
    with open(output_css, 'w') as f:
        f.write('/* ' + str(time.time()) + ' */\n')
        filters = filter_list.split('\n')
        css = ''
        for i in filters:
            if i.startswith('/*') or i.startswith('*/'):
                pass
            elif i.startswith('#'):
                css += convert_line(i) + ', \n'
        f.write(css[:-3] + ' ')
        f.write('''{
        display: none !important;
    }''')
