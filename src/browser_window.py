'''
Tunnel Browser

Browser Window
'''

# SPDX-License-Identifier: GPL-3.0-or-later

from io import BytesIO
import os, sys
import time
import mmap

import urllib
import requests

import threading

import subprocess

import gi
gi.require_version('Adw', '1')
from gi.repository import Gtk, GLib, WebKit, Gdk, GdkPixbuf, Gio, Pango, Adw

import sqlite3

from .download_window import DownloadWindow
from .settings_window import SettingsWindow

from .browser_window_ui import init_browser_window
from .browser_window_connections import set_connections
from .browser_window_funcs import *

BROWSER_VER = '2.0.0'

icons_path = __file__.rpartition(os.path.sep)[0] + '/data/'

START_PAGE = 'https://start.duckduckgo.com/'
COOKIES_PATH = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/cookies.txt')
SEARCH_URL = 'https://duckduckgo.com/?q='
HISTORY_FILE = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/history.db')
FILTER_PATH = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/filters.txt')
FILTER_CSS_PATH = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/filters.css')
BOOKMARKS_PATH = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/bookmarks.db')
SETTINGS_PATH = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/settings.db')
WINDOW_RESTORE_PATH = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/window_restore.txt')

class BrowserWindow(Adw.ApplicationWindow):
    def __init__(self, app, url = START_PAGE, incognito = False, adblocking = True):
        self.app = app

        self.incognito = incognito
        self.adblocking = adblocking

        self.HISTORY_FILE = HISTORY_FILE
        self.FILTER_CSS_PATH = FILTER_CSS_PATH
        self.FILTER_PATH = FILTER_PATH
        self.SETTINGS_PATH = SETTINGS_PATH

        if not self.incognito:
            Adw.ApplicationWindow.__init__(self, title="Tunnel", application = app)
        else:
            Adw.ApplicationWindow.__init__(self, title="Tunnel (Incognito)", application = app)

        url = get_navigable_url(url, SEARCH_URL)

        self.set_default_size(800, 600)

        if os.path.exists(WINDOW_RESTORE_PATH):
            restore_window = open(WINDOW_RESTORE_PATH, 'r').read()
            if restore_window == "True":
                self.maximize()

        self.set_icon_name('net.codelogistics.tunnel')

        self.webview = WebKit.WebView()
        self.manager = self.webview.get_user_content_manager()

        if not self.incognito:
            self.webview = WebKit.WebView()
            self.network_session = WebKit.NetworkSession.get_default()
            self.network_session.set_tls_errors_policy(0)
            context = self.webview.get_context()
            self.cookies = self.network_session.get_cookie_manager()
            storage = WebKit.CookiePersistentStorage.TEXT
            policy = WebKit.CookieAcceptPolicy.ALWAYS
            self.cookies.set_accept_policy(policy)
            self.cookies.set_persistent_storage(COOKIES_PATH, storage)

        else:
            self.webview = WebKit.WebView()
            self.network_session = WebKit.NetworkSession.get_default()
            self.network_session.set_tls_errors_policy(0)
            context = self.webview.get_context()
            self.cookies = self.network_session.get_cookie_manager()
            policy = WebKit.CookieAcceptPolicy.NEVER
            self.cookies.set_accept_policy(policy)            

        self.website_data_manager = self.network_session.get_website_data_manager()
        self.website_data_manager.set_favicons_enabled(True)

        self.favdb = self.website_data_manager.get_favicon_database()
        sm = context.get_security_manager()
        self.settings = self.webview.get_settings()
        user_agent = self.settings.get_property("user-agent")
        self.settings.set_property("user-agent", user_agent + ' Tunnel/' + BROWSER_VER.split('.')[0] + '.' + BROWSER_VER.split('.')[1]) # Use major version
        self.settings.set_property("enable-developer-extras", True)
        self.settings.set_enable_back_forward_navigation_gestures(True)
        self.settings.set_enable_webrtc(True)
        self.settings.set_javascript_can_access_clipboard(True)
        self.settings.set_enable_back_forward_navigation_gestures(True)

        if not os.path.exists(BOOKMARKS_PATH):
            con = sqlite3.connect(BOOKMARKS_PATH)
            cur = con.cursor()
            cur.execute("CREATE TABLE bookmarks (title text, uri text)")
            con.commit()
            con.close()

        init_browser_window(self, app = app, url = url, incognito = self.incognito)

        set_connections(self, app, HISTORY_FILE, START_PAGE)

        if self.adblocking and os.path.exists(FILTER_CSS_PATH):
                css_code = open(FILTER_CSS_PATH, 'r').read()
                style_sheet = WebKit.UserStyleSheet(css_code, 0, 0)
                self.manager.add_style_sheet(style_sheet)

        self.navigate(url)
        self.entry.grab_focus()

        con = sqlite3.connect(BOOKMARKS_PATH)
        cur = con.cursor()
        cur.execute('SELECT * FROM bookmarks')
        bookmarks = cur.fetchall()
        cur.close()
        for bookmark in bookmarks:
            if bookmark:
                #self.bm_store.append([self.get_cached_favicon(bookmark[1]), bookmark[0], bookmark[1]])
                pass

        update_thread = threading.Thread(target=update_filters, args=(self.adblocking, FILTER_PATH, FILTER_CSS_PATH))
        update_thread.start()

        completion = Gtk.EntryCompletion()

        self.hist_store = Gtk.ListStore(str)
        history = set()

        update_history_bar(self.hist_store)

        completion.set_model(self.hist_store)
        completion.set_text_column(0)
        completion.set_inline_completion(True)

        self.entry.set_completion(completion)

        self.connect("destroy", self.on_destroy)

        self.set_focus_child(None)

        app.create_action('caret', lambda x, y: self.settings.set_enable_caret_browsing(not self.settings.get_enable_caret_browsing()), ['F7'])

    def on_entry_activated(self, entry):
        url = get_navigable_url(entry.get_text(), SEARCH_URL)
        self.navigate(url)

    def on_load_progress(self, webview, progress):
        self.progressbar.set_fraction(webview.get_estimated_load_progress())

    def save_link_as(self, name, link):
        save_dialog = Gtk.FileChooserDialog("Save File", None, Gtk.FileChooserAction.SAVE, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
        save_dialog.set_default_response(Gtk.ResponseType.OK)
        save_dialog.set_current_name(name + '.html')
        save_dialog.set_current_folder(os.path.expanduser('~/Downloads'))
        response = save_dialog.run()
        if response == Gtk.ResponseType.OK:
            filename = save_dialog.get_filename()
            download_window = DownloadWindow(self.app, link, filename)
            download_window.show_all()
        save_dialog.destroy()

    def save_image_as(self, link):
        name = link.rstrip('/')
        name = name[name.rindex('/') + 1:]
        save_dialog = Gtk.FileChooserDialog("Save File", None, Gtk.FileChooserAction.SAVE, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
        save_dialog.set_default_response(Gtk.ResponseType.OK)
        save_dialog.set_current_name(name)
        save_dialog.set_current_folder(os.path.expanduser('~/Downloads'))
        response = save_dialog.run()
        if response == Gtk.ResponseType.OK:
            filename = save_dialog.get_filename()
            download_window = DownloadWindow(self.app, link, filename)
            download_window.show_all()
        save_dialog.destroy()

    def search_selected_text(self):
        self.file_html =  ''
        def on_file_save_finish(webview, result, user_data=None):
            file_html = self.webview.run_javascript_finish(result).get_js_value().to_string()
            self.file_html = file_html
        self.webview.run_javascript("window.getSelection().getRangeAt(0).toString()", None, on_file_save_finish, None)
        get_select_th = threading.Thread(target=self.get_selected_text_target)
        get_select_th.start()        

    def get_selected_text_target(self):
        start_time = time.time()
        while self.file_html == '':
            time.sleep(0.01)
            if time.time() - start_time > 0.5:
                break # Timeout
            pass
        if self.file_html != '':
            self.navigate(SEARCH_URL + self.file_html)

    def on_context_menu(self, webview, context_menu, event, hit_test_result):
        if hit_test_result.context_is_link(): # Remove default Save Link As
            context_menu.remove(context_menu.get_item_at_position(2))

        if hit_test_result.context_is_link(): # Add Save Link As (new)
            save_link_as_action = Gtk.Action(name="save_link_as_action", label="Save Link As")
            name = hit_test_result.get_link_label()
            if not name:
                name = hit_test_result.get_link_title()
            if not name:
                name = get_domain_name(hit_test_result.get_link_uri())
            if not name:
                name = "Document"
            save_link_as_action.connect('activate', lambda action: self.save_link_as(name, hit_test_result.get_link_uri()))
            m_item = WebKit.ContextMenuItem.new(save_link_as_action)
            context_menu.insert(m_item, 2)

        if hit_test_result.context_is_selection() and not hit_test_result.context_is_image():
            search_action = Gtk.Action(name="search_text_action", label="Search selected text")
            search_action.connect('activate', lambda action: self.search_selected_text())
            m_item = WebKit.ContextMenuItem.new(search_action)
            context_menu.insert(m_item, 0)
        
        if hit_test_result.context_is_image() and hit_test_result.context_is_link(): # Remove Save Image As
            context_menu.remove(context_menu.get_item_at_position(6))
        elif hit_test_result.context_is_image() and not hit_test_result.context_is_link():
            context_menu.remove(context_menu.get_item_at_position(1))
        
        if hit_test_result.context_is_image(): # Add Save Image As (new)
            save_image_as_action = Gtk.Action(name="save_image_as_action", label="Save Image As")
            save_image_as_action.connect('activate', lambda action: self.save_image_as(hit_test_result.get_image_uri()))
            m_item = WebKit.ContextMenuItem.new(save_image_as_action)
            if hit_test_result.context_is_link():
                context_menu.insert(m_item, 6)
            else:
                context_menu.insert(m_item, 1)

        save_page_action = Gtk.Action(name="save_page_action", label="Save Page")
        save_page_action.connect('activate', lambda action: self.on_save_button_clicked(action))
        m_item = WebKit.ContextMenuItem.new(save_page_action)
        context_menu.append(m_item)

        select_all_action = Gtk.Action(name="select_all_action", label="Select All")
        select_all_action.connect('activate', lambda action: self.webview.execute_editing_command(WebKit.EDITING_COMMAND_SELECT_ALL))
        m_item = WebKit.ContextMenuItem.new(select_all_action)
        context_menu.insert(m_item, 0)
        
    '''def show_bookmarks_bar(self, widget, action=None):
        self.bookmarks_bar.set_visible(not self.bookmarks_bar.get_visible())

    def add_rm_bookmarks(self, add = False, delete = False):
        if add:
            con = sqlite3.connect(BOOKMARKS_PATH)
            cur = con.cursor()
            cur.execute('INSERT INTO bookmarks VALUES (?, ?)', (self.webview.get_title(), self.webview.get_uri()))
            con.commit()
            cur.close()
            #self.bm_store.append([self.get_cached_favicon(self.webview.get_uri()), self.webview.get_title(), self.webview.get_uri()])
            
        if delete:
            selection = self.bm_list.get_selection()
            model, treeiter = selection.get_selected()
            if treeiter is not None:
                con = sqlite3.connect(BOOKMARKS_PATH)
                cur = con.cursor()
                cur.execute('DELETE FROM bookmarks WHERE uri = ?', (model.get_value(treeiter, 2),))
                con.commit()
                cur.close()
            model.remove(treeiter)
    
    def update_bookmarks(self):
        con = sqlite3.connect(BOOKMARKS_PATH)
        cur = con.cursor()
        cur.execute('DELETE FROM bookmarks')
        #self.bm_store.foreach(lambda model, path, treeiter: self.write_bm(model, path, treeiter, cur))
        con.commit()
        cur.close()
    
    def write_bm(self, model, path, treeiter, cur):
        cur.execute('INSERT INTO bookmarks VALUES (?, ?)', (model[path][1], model[path][2]))

    def on_row_activated(self, treeview, path, column):
        model = treeview.get_model()
        treeiter = model.get_iter(path)
        value = model.get_value(treeiter, 2)
        self.navigate(value)
        
    def on_cell_edited(self, renderer, path, new_text, column, model):
        model[path][column] = new_text
        self.update_bookmarks()'''

    def bookmark(self):
        pass

    def set_new_favicon(self, webview, favicon):
        texture = self.webview.get_favicon()
        if texture == None:
            self.favicon_image.set_visible(False)
            return
        else:
            self.favicon_image.set_visible(True)

        texture_bytes = texture.save_to_png_bytes()
        input_stream = Gio.MemoryInputStream.new_from_bytes(texture_bytes)
        pixbuf = GdkPixbuf.Pixbuf.new_from_stream(input_stream, None)
        pixbuf = pixbuf.scale_simple(22, 22, GdkPixbuf.InterpType.BILINEAR)
        self.favicon_image.set_from_pixbuf(pixbuf)

    def get_cached_favicon(self, url) -> GdkPixbuf.Pixbuf:
        def set_cached_favicon(favicon):
            def set_cached_favicon_cb(favdb, result):
                try:
                    texture = self.favdb.get_favicon_finish(result)
                except:
                    return

                texture_bytes = texture.save_to_png_bytes()
                input_stream = Gio.MemoryInputStream.new_from_bytes(texture_bytes)
                pixbuf = GdkPixbuf.Pixbuf.new_from_stream(input_stream, None)
                pixbuf = pixbuf.scale_simple(22, 22, GdkPixbuf.InterpType.BILINEAR)
                pixbuf.copy_area(0, 0, 22, 22, favicon, 0, 0)
                
            self.favdb.get_favicon(url, callback = set_cached_favicon_cb)

        favicon = GdkPixbuf.Pixbuf.new(GdkPixbuf.Colorspace.RGB, True, 8, 22, 22)
        set_cached_favicon_thread = threading.Thread(target=set_cached_favicon, args=(favicon,))
        set_cached_favicon_thread.start()
        return favicon

    def on_back_clicked(self, button):
        self.webview.go_back()

    def on_forward_clicked(self, button):
        self.webview.go_forward()
    
    def on_reload_clicked(self, button):
        if button.get_tooltip_text() == 'Stop':
            self.webview.stop_loading()
            button.set_icon_name("arrow-circular-top-right-symbolic")
            button.set_tooltip_text('Reload')
            self.progress.set_visible(False)
        else:
            self.webview.reload()
            button.set_icon_name("arrow-circular-top-right-symbolic")
            button.set_tooltip_text('Stop')

    def on_menu_button_clicked(self, button):
        self.menu.popup_at_pointer(None)

    def create_new_window(self, app, url = START_PAGE, incognito = False):

        new_window = BrowserWindow(app, url, incognito=incognito)
        new_window.present()
        new_window.find_bar.set_visible(False)
        new_window.bookmarks_bar.set_visible(False)

    def on_save_button_clicked(self, button, action = None):
        self.file_html =  ''
        def on_file_save_finish(webview, result, user_data=None):
            def on_file_dialog_finish(dialog, result, user_data=None):
                try:
                    file = save_dialog.save_finish(result)
                except:
                    return
                filename = file.get_path()
                with open(filename, 'w') as f:
                    f.write(file_html)
            file_html = self.webview.evaluate_javascript_finish(result).to_string()
            save_dialog = Gtk.FileDialog()
            save_dialog.set_initial_name(str(self.webview.get_title()) + '.html')
            save_dialog.set_initial_folder(Gio.File.new_for_path(os.path.expanduser('~/Downloads')))
            response = save_dialog.save(self, None, on_file_dialog_finish, None)
            
        self.webview.evaluate_javascript("document.documentElement.outerHTML", -1, None, None, None, on_file_save_finish, None)

    def on_print_button_clicked(self, button, action = None):
        self.webview.evaluate_javascript("window.print()", -1)

    def on_hist_button_clicked(self, button, action = None):
        if os.path.exists(HISTORY_FILE):
            con = sqlite3.connect(HISTORY_FILE)
            cur = con.cursor()
            cur.execute("SELECT * FROM history")
            history = cur.fetchall()
            con.close()
            hist_html = "<style>td {word-break:break-all; width: 50%;border:1px solid black}</style><body><h2>History</h2><hr><table style=\"width:100%; table-layout:fixed; border-collapse: collapse;\"><thead><tr><th>URL</th><th>Time</th></tr></thead><tbody>"
            for i in history[::-1]:
                if i[1] == '' or i[1] is None:
                    if len(i[0]) > 50:
                        show_url = i[0][:50] + '...'
                    else:
                        show_url = i[0]
                else:
                    if len(i[1]) > 50:
                        show_url = i[1][:50] + '...'
                    else:
                        show_url = i[1]

                hist_html += '<tr><td><a href="' + i[0] + '">' + show_url + '</a></td><td>' + i[2] + '</td></tr>'
            hist_html += '</tbody></body></table>'
            self.webview.load_html(hist_html)
        else:
            dialog = Gtk.MessageDialog(parent=None, message_type=Gtk.MessageType.ERROR, buttons=Gtk.ButtonsType.OK, text='There is no browsing history to show yet.')
            dialog.run()
            dialog.destroy()

    def on_clear_hist_button_clicked(self, button, action = None):
        os.remove(HISTORY_FILE)
        self.navigate(START_PAGE)
        con = sqlite3.connect(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/history.db'))
        cur = con.cursor()
        cur.execute('CREATE TABLE IF NOT EXISTS history (url varchar(1024), title varchar(255), datetime varchar(50));')
        con.commit()

    def show_find_bar(self, button, action=None):
        self.find_bar.set_visible(True)
        self.find_entry.set_can_focus(True)
        self.find_entry.grab_focus()

    def find_text(self, button):
        if self.find_entry.get_text() != '':
            self.find_controller = self.webview.get_find_controller()
            find_flags = 4
            if not self.case_sensitive_button.get_active():
                find_flags += 1
            if self.wrap_button.get_active():
                find_flags += 16
            if self.word_button.get_active():
                find_flags += 2
            if self.backwards_button.get_active():
                find_flags += 8
            self.find_controller.search(self.find_entry.get_text(), find_flags, 1000)

    def close_find_bar(self, button):
        try:
            self.find_controller.search_finish()
        except:
            pass
        self.find_bar.set_visible(False)

    def load_page(self, url):
        GLib.idle_add(self.webview.load_uri, url)

    def navigate(self, url):
        self.entry.set_text(url)
        load_webpage_thread = threading.Thread(target=self.load_page, args=(url,))
        load_webpage_thread.start()
        self.set_focus_child(None)

    def show_settings_dialog(self, widget, app):
        settings_dialog = SettingsWindow(app)
        settings_dialog.present()

    def show_about_dialog(self, widget):
        about_dialog = Gtk.AboutDialog()
        about_dialog.set_icon_name('net.codelogistics.tunnel')
        about_dialog.set_program_name("Tunnel")
        about_dialog.set_version(BROWSER_VER)
        about_dialog.set_comments("A lightweight, minimalist browser for browsing the Web.\nBased on WebKit, WebKitGTK and GTK4.")
        about_dialog.set_license_type(Gtk.License.GPL_3_0)
        about_dialog.set_website("https://tunnel.codelogistics.net/")
        about_dialog.set_authors(["Satvik Patwardhan", "This project also uses work by:", "Steven Black (Adblock domains filter list)", "EasyList (Adblock domains filter list)"])
        about_dialog.set_artists(["Tango Icon Library"])
        about_dialog.set_logo_icon_name('net.codelogistics.tunnel')
        about_dialog.present()
    
    def on_destroy(self, window):
        with open(WINDOW_RESTORE_PATH, 'w') as restore_file:
            restore_file.write(str(self.is_maximized()))
        # Window position and size cannot be gotten reliably.
        self.webview.terminate_web_process()
