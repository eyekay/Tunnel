'''
Tunnel Browser

Browser Window - WebView signals
'''

# SPDX-License-Identifier: GPL-3.0-or-later

import os
import subprocess
import sqlite3

import gi

from gi.repository import Gtk, WebKit

from .browser_window_funcs import *
from .download_window import DownloadWindow

icons_path = __file__.rpartition(os.path.sep)[0] + '/data/'

def on_load_changed(self, webview, event, HISTORY_FILE, FILTER_CSS_PATH):         
        if self.webview.can_go_back():
            self.backbutton.set_sensitive(True)
        else:
            self.backbutton.set_sensitive(False)
        if self.webview.can_go_forward():
            self.forwardbutton.set_sensitive(True)
        else:
            self.forwardbutton.set_sensitive(False)
        if event == WebKit.LoadEvent.STARTED:
            self.progress.set_visible(True)
            self.reloadbutton.set_icon_name("cross-large-symbolic")
            self.reloadbutton.set_tooltip_text('Stop')
        elif event == WebKit.LoadEvent.COMMITTED:
            self.entry.set_text(webview.get_uri())
        elif event == WebKit.LoadEvent.FINISHED:
            history = set()
            history.add(self.webview.get_uri())
            history.add(remove_if_starts_with(remove_if_starts_with(remove_if_starts_with(self.webview.get_uri(), 'https://'), 'http://'), 'www.'))
            history.add(remove_if_starts_with(remove_if_starts_with(webview.get_uri(), 'https://'), 'http://'))
            bare_domain = remove_if_starts_with(remove_if_starts_with(webview.get_uri(), 'https://'), 'http://').split('.')
            if len(bare_domain) > 2:
                history.add(bare_domain[-2] + '.' + bare_domain[-1])
            history.add(get_domain_name(self.webview.get_uri()))

            for i in list(history)[::-1]:
                self.hist_store.append([i])

            self.progress.set_visible(False)
            self.reloadbutton.set_icon_name("arrow-circular-top-right-symbolic")
            self.reloadbutton.set_tooltip_text('Reload')
            
            if self.webview.get_uri() != 'about:blank' and not self.network_session.is_ephemeral():
                con = sqlite3.connect(HISTORY_FILE)
                cur = con.cursor()
                cur.execute('INSERT INTO history (url, title, datetime) VALUES (?, ?, ?)', (self.webview.get_uri(), self.webview.get_title(), time.strftime('%Y-%m-%d %H:%M:%S')))
                con.commit()
                con.close()

            try:
                self.set_title(self.webview.get_title() + ' - Tunnel')
            except: # if the link cannot be opened get_title returns NoneType.
                self.set_title('Tunnel')
        self.set_focus_child(None)
        self.webview.grab_focus()
        
def on_decide_policy(self, app, web_view, decision, decision_type, FILTER_PATH):
    if decision_type == WebKit.PolicyDecisionType.RESPONSE:
        content_type = decision.get_response().get_http_headers().get_content_type()
        uri = decision.get_response().get_uri()

        if uri == "about:blank":
            decision.ignore()
            return
        elif uri.startswith('file://'):
            decision.ignore()
            return
        elif not uri.startswith('http'):
            decision.ignore()
            # Open the link in the system app
            if sys.platform == "darwin":
                subprocess.Popen(['open', uri])
            elif sys.platform == "linux":
                subprocess.Popen(['xdg-open', uri])
            elif sys.platform == "win32":
                subprocess.Popen(["start", uri], shell=True)
            return
        elif self.adblocking and os.path.exists(FILTER_PATH) and len(open(FILTER_PATH, 'r').read()) != 0 and is_to_be_filtered('0.0.0.0 ' + get_domain_name(uri) + '\n', FILTER_PATH):
            decision.ignore()
            return
        elif not self.webview.can_show_mime_type(content_type[0]):
            file_dialog = Gtk.FileChooserDialog("Save File", self, Gtk.FileChooserAction.SAVE, (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_SAVE, Gtk.ResponseType.OK))
            file_dialog.set_default_response(Gtk.ResponseType.OK)
            #file_dialog.set_icon_from_file(icons_path + 'tunnel_icon')
            file_dialog.set_current_name(uri.split('/')[-1])
            file_dialog.set_current_folder(os.path.expanduser('~/Downloads'))
            response = file_dialog.run()
            if response == Gtk.ResponseType.OK:
                path = file_dialog.get_uri()[7:]
                file_dialog.destroy()
            else:
                file_dialog.destroy()
                return
            download_window = DownloadWindow(app, uri, path)
            download_window.show_all()

def on_permission_request(self, webview, request, SETTINGS_PATH):
    if not os.path.exists(SETTINGS_PATH):
        con = sqlite3.connect(SETTINGS_PATH)
        cur = con.cursor()
        cur.execute("CREATE TABLE IF NOT EXISTS settings (url TEXT, setting TEXT, value BOOLEAN)")
        con.commit()
        con.close()

    def update_settings(url, setting, value):
        con = sqlite3.connect(SETTINGS_PATH)
        cur = con.cursor()
        cur.execute("SELECT setting, value FROM settings WHERE url = ?", (get_domain_name(webview.get_uri()),))
        settings = cur.fetchall()
        found = False
        if settings:
            for i in settings:
                if i[0] == setting:
                    found = True
                else:
                    pass
        if found:
            cur.execute("UPDATE settings SET value = ? WHERE url = ?", (value, get_domain_name(webview.get_uri())))
        else:
            cur.execute("INSERT INTO settings (url, setting, value) VALUES (?, ?, ?)", (get_domain_name(webview.get_uri()), setting, value))
        con.commit()
        con.close()

    def handle_perms(setting, message):
        con = sqlite3.connect(SETTINGS_PATH)
        cur = con.cursor()
        cur.execute("SELECT setting, value FROM settings WHERE url = ?", (get_domain_name(webview.get_uri()),))
        settings = cur.fetchall()
        con.close()
        if settings:
            for i in settings:
                if i[0] == setting:
                    if i[1]:
                        request.allow()
                        return
                    else:
                        request.deny()
                        return
                else:
                    pass


        dialog = Gtk.MessageDialog(
            parent=self,
            modal=True,
            type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            message_format="Do you want to allow {} access to {}?".format(webview.get_uri(), message)
        )
        response = dialog.run()
        dialog.destroy()
        if response == Gtk.ResponseType.YES:
            request.allow()
        else:
            request.deny()
        remember_dialog = Gtk.MessageDialog(
            parent=self,
            modal=True,
            type=Gtk.MessageType.QUESTION,
            buttons=Gtk.ButtonsType.YES_NO,
            message_format="Do you want to store your settings about access to {}?".format(message)
        )
        remember_response = remember_dialog.run()
        remember_dialog.destroy()
        if remember_response == Gtk.ResponseType.YES:
            if response == Gtk.ResponseType.YES:
                update_settings(get_domain_name(webview.get_uri()), setting, True)
            else:
                update_settings(get_domain_name(webview.get_uri()), setting, False)
        else:
            pass

    if type(request) == WebKit.UserMediaPermissionRequest:
        handle_perms('media', 'your microphone and camera')
    elif type(request) == WebKit.DeviceInfoPermissionRequest:
        handle_perms('deviceinfo', 'your microphone and camera information')
    elif type(request) == WebKit.GeolocationPermissionRequest:
        handle_perms('geolocation', 'your location')
    elif type(request) == WebKit.MediaKeySystemPermissionRequest:
        handle_perms('mediakey', 'installing or use of DRM software')
    elif type(request) == WebKit.NotificationPermissionRequest:
        handle_perms('notification', 'sending notifications')
    elif type(request) == WebKit.WebsiteDataAccessPermissionRequest:
        request.deny()


def on_create(wv, navigationaction, self, app):
    self.create_new_window(app, navigationaction.get_request().get_uri(), False)