'''
Tunnel Browser

Lightweight Gtk3 Web Browser
'''

# SPDX-License-Identifier: GPL-3.0-or-later

import os, sys

import urllib

import gi

gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
gi.require_version('WebKit', '6.0')
from gi.repository import Gtk, Gio, WebKit, Adw

from .browser_window import BrowserWindow

BROWSER_VER = '2.0.0'

if os.path.exists(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel')):
    if os.path.exists(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/favicons')):
        pass
    else:
        os.mkdir(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/favicons'))
else:
    os.mkdir(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel'))
    os.mkdir(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/favicons'))

incognito = False
adblocking = True

START_PAGE = 'https://start.duckduckgo.com/'
COOKIES_PATH = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/cookies.txt')
SEARCH_URL = 'https://duckduckgo.com/?q='
HISTORY_FILE = os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/history.db')

if os.path.exists(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/prefs.conf')):
    prefs = open(os.path.expanduser('.var/app/net.codelogistics.tunnel/.tunnel/prefs.conf'), 'r').read().split('\n')
    for i in prefs:
        if i.startswith('start_page'):
            START_PAGE = i.partition('=')[2].strip()
        if i.startswith('cookies_path'):
            COOKIES_PATH = i.partition('=')[2].strip()
        if i.startswith('search_url'):
            SEARCH_URL = i.partition('=')[2].strip()
        if i.startswith('history_page'):
            HISTORY_FILE = i.partition('=')[2].strip()
        if i.startswith('incognito'):
            incognito = i.split('=')[1].strip()
            if incognito == "True":
                incognito = True
            else:
                incognito = False
        if i.startswith('adblocking'):
            adblocking = i.split('=')[1].strip()
            if adblocking == "True":
                adblocking = True
            else:
                adblocking = False

class TunnelApp(Adw.Application):
    def __init__(self, url, incognito=False, adblocking = True):
        Adw.Application.__init__(self)
        Gio.Application.set_flags(self, Gio.ApplicationFlags.NON_UNIQUE)
        self.args = [url, incognito, adblocking]
        Gio.Application.set_application_id(self, "net.codelogistics.tunnel")

    def do_activate(self):
        if self.args[0] == START_PAGE:
            window = BrowserWindow(self, self.args[0], incognito= self.args[1], adblocking=self.args[2])
        else:
            window = BrowserWindow(self, self.args[0], incognito= self.args[1], adblocking=self.args[2])
        window.present()
        window.find_bar.set_visible(False)
    
    def create_action(self, name, callback, shortcuts=None):
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)
        if shortcuts:
            self.set_accels_for_action(f"app.{name}", shortcuts)

    def do_startup(self):
        Adw.Application.do_startup(self)

if '--incognito' in sys.argv:
    incognito=True
if '--no-adblock' in sys.argv:
    adblocking=False

if '-v' in sys.argv or '--version' in sys.argv:
    print('Tunnel ' + BROWSER_VER)
    sys.exit()
elif '-h' in sys.argv or '--help' in sys.argv:
    print('''Tunnel {}
Usage: tunnel [OPTIONS] URL

Options:
    -h, --help
    -v, --version
    --incognito
    --no-adblock
'''.format(BROWSER_VER))
    sys.exit()

if len(sys.argv) > 1 and sys.argv[-1].startswith('-') != True:
    url = sys.argv[-1]
else:
    url = START_PAGE

app = TunnelApp(url = url, incognito=incognito, adblocking=adblocking)

exit_status = app.run([])
