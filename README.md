# Tunnel

Tunnel is a simplistic, lightweight and minimalist web browser based on the principle that even if you don't use the internet very often, using Tunnel is probably still not a very good idea. It is one of the premier browsers for downloading other browsers, continuing the longlasting legacy of the ineffectiveness and uselessness of several of its predecessors.

Tunnel is a suitable alternative to popular browsers for old or less powerful hardware. It is inspired by suckless.org's `surf` and the old Midori browser.

![Screenshot of Tunnel](screenshot.png)

## Features

* **Lightweight**: Less than half an MB in size (not counting dependencies), and about half the memory usage of Firefox.*
* **Ad Blocking**: It do.
* **Fast to launch**: Because of the lack of features, Tunnel is relatively quick to open.
* **Minimalist**: The interface will be familiar users who have previously been exposed to web browsers.
* **Incognito**: Can be enabled by default as well.
* **Downloads**: Can download files. It does not remember what it has downloaded once it is done, though.
* **Search**: It do.
* **Not a Chromium wrapper**: Uses WebKit, which is good because it isn't Blink.
* **URL bar**: With internet search and autofill for previously accessed addresses.

\* *Very rough calculations. Do not trust.*

## Anti-Features

* **Security**: *Questionable.* It stores cookies and browsing history unencrypted (to be fair, Firefox stores cookies unencrypted as well).
* **Tabs**: Not present. You can open multiple windows instead, which is the way god intended.
* **Passwords**: Not managed. Use an actual password manager. But you don't have to login each time you open the browser as that is done by cookies.

## Command-line options

Incantation: `flatpak run net.codelogistics.tunnel [OPTIONS] URL`

--incognito

--no-adblock

## Todo

* Block and allow domains
* Store various data using webkitwebsitedatamanager
* Use content filter blockers for ad blocking

## NVIDIA users

If something goes wrong with playing videos or displaying graphics, try using Nouveau drivers.

## License and Credits

This is a program designed to add the word 'Tunnel' and an icon to the user's app menu. Any web browsers included in the app are purely coincidental.

This program comes with absolutely no warranty. See the [GNU General Public License, version 3 or later](https://www.gnu.org/licenses/gpl-3.0.html) for details.

This program uses [StevenBlack Unified Hosts](https://github.com/StevenBlack/hosts) for domain-based  and [EasyList](https://easylist.to/easylist/easylist.txt) for CSS element based ad blocking.

Made by [Satvik Patwardhan](https://codelogistics.net)
